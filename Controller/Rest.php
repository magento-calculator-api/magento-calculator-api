<?php
namespace Rezolve\Calculator\Controller;

use Magento\Framework\Oauth\Helper\Request;

class Rest
{
    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    private $objectManager;
    /**
     * @var \Magento\Webapi\Controller\Rest\InputParamsResolver
     */
    private $inputParamsResolver;
    /**
     * @var \Rezolve\Calculator\Api\CalculatorInterface
     */
    private $calculatorInterface;

    /**
     * Rest constructor.
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     * @param \Magento\Webapi\Controller\Rest\InputParamsResolver $inputParamsResolver
     * @param \Rezolve\Calculator\Api\CalculatorInterface $calculatorInterface
     */
    public function __construct(
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Webapi\Controller\Rest\InputParamsResolver $inputParamsResolver,
        \Rezolve\Calculator\Api\CalculatorInterface $calculatorInterface
    ) {
        $this->objectManager        = $objectManager;
        $this->inputParamsResolver  = $inputParamsResolver;
        $this->calculatorInterface  = $calculatorInterface;
    }

    /**
     * @param \Magento\Webapi\Controller\Rest $rest
     * @param $result
     * @return mixed
     */
    public function afterDispatch(\Magento\Webapi\Controller\Rest $rest, $result)
    {
        $route = $this->inputParamsResolver->getRoute();
        $serviceClassName = $route->getServiceClass();
        if ($serviceClassName == 'Rezolve\Calculator\Api\CalculatorInterface') {
            $serviceMethodName = $route->getServiceMethod();
            $inputParams = $this->inputParamsResolver->resolve();
            $outputData = call_user_func_array([$this->calculatorInterface, $serviceMethodName], $inputParams);
            $result->setHttpResponseCode(Request::HTTP_OK);
            $result->setHeader('Content-type', 'application/json', true);
            $result->setBody($outputData);
        }
        return $result;
    }
}
